import utils.AlgorithmManager;
import utils.Generator;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class Main {

    public static void main(String[] args) {

        AlgorithmManager manager = new AlgorithmManager(1);
        manager.initRead();
        System.out.println(manager);
        manager.runExactAlgorithm();
        manager.runHeuristicAlgorithm();

    }
}
