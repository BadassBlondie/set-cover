package algorithms;

import collection.CollectionS;
import collection.CollectionU;
import collection.Outcome;
import utils.Stats;

import java.util.Collections;
import java.util.List;

public class ExactAlgo {

    private CollectionU collectionU;
    private List<CollectionS> collectionS;
    private List<Outcome> outcomes; //wynikowa lista
    private Long time,memory,counter; //zmienne do statystyk, counter ilosc wykonanych instrukcji


    public ExactAlgo(CollectionU collectionU, List<CollectionS> collectionS) {
        this.collectionS=collectionS;
        this.collectionU=collectionU;
    }


    public void run() {
        initBeforeStart();
        solve();
        endProcessing();
    }

    private void solve() {
        int size = collectionS.size(); //rozmiar listy z setami
        Outcome result = new Outcome();
        Outcome generated; //wynikowa lista z kandydatami do pokrycia
        for (int i = 0; i < (1 << size); i++) { //przesuniecie bitowe w lewo poniewaz 2^n kombinacji n elementowego zbioru
            generated = new Outcome();

            for (int j = 0; j < size; j++) { //sprawdzamy kazdy set z kazdym
                if ((i & (1 << j)) > 0) {
                    generated.addSet(collectionS.get(j));
                }
            }
            counter+=size;
            if (generated.checkCoverage(collectionU)) {
                counter+=1;
                //sprawdzamy moc zbioru i wybieramy najmniejszy zestaw setow pokrywajacych zbior
                if (generated.getSets().size() < result.getSets().size() || result.getSets().size() == 0) {
                    result = generated;
                    counter+=1;
                }
            }
        }
        outcomes = Collections.singletonList(result);
    }

    private void initBeforeStart(){
        System.out.println(String.format("Starting %s", getClass().getSimpleName()));
        counter = 0L;
        memory = Stats.getUsedMemory();
        time = System.currentTimeMillis();
    }
    private void endProcessing() {
        time = System.currentTimeMillis() - time;
        memory = Stats.getUsedMemory() - memory;
        System.out.println("Founded solution sets:");
        for (Outcome o:outcomes ) {
            System.out.println(o);
            System.out.println("Solution has been found with coverage by "+o.getSets().size()+" sets");
        }
        System.out.println();
        System.out.println("The "+ getClass().getSimpleName()+" has finished.");
        System.out.println("================ STATS ================");
        System.out.println(String.format("Done in %d millis.", time));
        System.out.println(String.format("Used %d bytes of memory.", memory));
        System.out.println(String.format("Executed %d instructions", counter));
        System.out.println("================END STATS ================");


    }

}
