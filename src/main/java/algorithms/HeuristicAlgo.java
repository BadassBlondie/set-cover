package algorithms;

import collection.CollectionS;
import collection.CollectionU;
import collection.Outcome;
import utils.CoverError;
import utils.Stats;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class HeuristicAlgo {

    private CollectionU collectionU;
    private List<CollectionS> collectionS;
    private List<Outcome> outcomes; //wynikowa lista
    private Long time,memory,counter; //zmienne do statystyk, counter ilosc wykonanych instrukcji


    public HeuristicAlgo(CollectionU collectionU, List<CollectionS> collectionS) {
        this.collectionS=collectionS;
        this.collectionU=collectionU;
    }


    public void run() {
        initBeforeStart();
        solve();
        endProcessing();
    }

    private void solve() {
        Outcome outcome = new Outcome();
        List<CollectionS> notChosenSets = new ArrayList<>(collectionS);

        do{
            CollectionS effSet = findMostEff(notChosenSets, outcome);
            outcome.addSet(effSet);
            notChosenSets.remove(effSet);
            counter+=3;
        }while (!outcome.checkCoverage(collectionU)) ;

        outcomes = Collections.singletonList(outcome);
    }

    private CollectionS findMostEff(List<CollectionS> sets, Outcome outcome){
        List<Integer> covered = new ArrayList<>();

        for (CollectionS s:outcome.getSets()) {//zapisanie elementów do C
            covered.addAll(s.getElements());
            counter+=1;
        }

        int effSet = 0;
        int effOfChosenSet = 0;
        int setEff;
        if(sets.size()<=0){
            throw new CoverError();
        }

        for (int i = 0; i < sets.size(); i++) { //liczenie eff po wszystkich nie wybranyh zbiorach
            setEff = coverage(covered, sets.get(i).getElements());
            counter+=1;

            if(effOfChosenSet < setEff){
                effSet = i;
                effOfChosenSet = setEff;
            }
        }

        return sets.get(effSet);
    }

    private Integer coverage(List<Integer> covered, List<Integer> setElements){
        List<Integer> setsValues = new ArrayList<>(setElements);
        setsValues.removeAll(covered);
        return setsValues.size();
    }

    private void initBeforeStart(){
        System.out.println(String.format("\nStarting %s", getClass().getSimpleName()));
        counter = 0L;
        memory = Stats.getUsedMemory();
        time = System.currentTimeMillis();
    }
    private void endProcessing() {
        time = System.currentTimeMillis() - time;
        memory = Stats.getUsedMemory() - memory;
        System.out.println("Founded solution sets:");
        for (Outcome o:outcomes ) {
            System.out.println(o);
            System.out.println("Solution has been found with coverage by "+o.getSets().size()+" sets");
        }
        System.out.println();
        System.out.println("The "+ getClass().getSimpleName()+" has finished.");
        System.out.println("================ STATS ================");
        System.out.println(String.format("Done in %d millis.", time));
        System.out.println(String.format("Used %d bytes of memory.", memory));
        System.out.println(String.format("Executed %d instructions", counter));
        System.out.println("================END STATS ================");
    }
}
