package collection;

import java.util.List;
import java.util.stream.Collectors;

public class CollectionS {

    private Integer id;
    private List<Integer> elements;

    private static Integer AMOUNT = 0;

    public static List<CollectionS> create(List<List<Integer>> subsets) {
        return subsets.stream()
                .map(e -> {
                    CollectionS set = new CollectionS();
                    set.setId(++AMOUNT);
                    set.setElements(e);
                    return set;
                }).collect(Collectors.toList());
    }

    private CollectionS() {
    }

    Integer getId() {
        return id;
    }

    private void setId(Integer id) {
        this.id = id;
    }

    public List<Integer> getElements() {
        return elements;
    }

    public Integer getSize(){ return elements.size(); }

    private void setElements(List<Integer> elements) {
        this.elements = elements;
    }

    @Override
    public String toString() {
        return "Set " + id + ": " + elements;
    }
}
