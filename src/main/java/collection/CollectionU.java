package collection;

import java.util.List;

public class CollectionU {

    private List<Integer> elements;

    public CollectionU() {
    }

    public static CollectionU create(List<Integer> elements){
       CollectionU collectionU = new CollectionU();
       collectionU.setElements(elements);
       return collectionU;
    }

    private void setElements(List<Integer> elements){
        this.elements = elements;
    }

    public List<Integer> getElements() {
        return elements;
    }
}
