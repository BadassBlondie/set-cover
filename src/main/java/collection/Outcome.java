package collection;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Outcome {
    private List<CollectionS> sets = new ArrayList<>();

    public void addSet(CollectionS set) {
        sets.add(set);
    }


    public boolean checkCoverage(CollectionU collectionU){
        List<Integer> listOfElements = new ArrayList();
        for (CollectionS s:sets) {//extract elements
            listOfElements.addAll(s.getElements());
        }
        return listOfElements.containsAll(collectionU.getElements());
    }

    public List<CollectionS> getSets() {
        return sets;
    }

    public CollectionS getSet(int index){return  sets.get(index); }

    @Override
    public String toString() {
        List<String> setNames = sets.stream()
                .map(s -> "Set " + s.getId() + " " + s.getElements())
                .collect(Collectors.toList());
        return String.join(",\n", setNames);
    }
}
