package utils;

import algorithms.ExactAlgo;
import algorithms.HeuristicAlgo;
import collection.CollectionS;
import collection.CollectionU;

import java.util.List;

public class AlgorithmManager {

    CollectionU collectionU;
    List<CollectionS> collectionS;

    private ExactAlgo exactAlgo;
    private HeuristicAlgo heuristicAlgo;
    private int flag; //0 if from file, 1 from generator


    public AlgorithmManager(int generator) {
        flag=generator;
    }

    public void initRead(){
        if (flag == 1) {
            collectionU = CollectionU.create(Generator.initU());
            collectionS = CollectionS.create(Generator.initS());
        } else {
            collectionU = CollectionU.create(ReadFile.initU());
            collectionS = CollectionS.create(ReadFile.initS());
        }

    }

    public void runExactAlgorithm(){
        exactAlgo = new ExactAlgo( collectionU, collectionS);
        exactAlgo.run();
    }

    public void runHeuristicAlgorithm(){
        heuristicAlgo = new HeuristicAlgo(collectionU, collectionS);
        heuristicAlgo.run();
    }


    @Override
    public String toString() {
        return "InitManager{" +
                "collectionU=" + collectionU.getElements() +
                ",\ncollectionS=" + collectionS +
                '}';
    }
}
