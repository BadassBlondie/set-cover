package utils;

public class CoverError extends RuntimeException{
    public CoverError(){
        super("Coverage of the given U is impossible!");
    }
}
