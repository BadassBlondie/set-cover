package utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

public class Generator {

    private static final Integer SIZE_OF_U = 20;
    private static final Integer NUM_OF_SETS = 20;
    private static final Integer LEVEL_OF_COVERAGE = 2;
    private static final ThreadLocalRandom random = ThreadLocalRandom.current();

    private static List<Integer> collectionU;


    public static List<Integer> initU() {
        Set<Integer> collU = new HashSet<>();
        while (collU.size() < SIZE_OF_U) {
            collU.add(random.nextInt(SIZE_OF_U * 5));
        }
        collectionU =new ArrayList<>(collU);
       return collectionU;
    }

    public static List<List<Integer>> initS() {
        Set<Set<Integer>> sets = new HashSet<>();

        if(collectionU.size()>0) {
            while (sets.size() < NUM_OF_SETS) {
                sets.add(generateSingleSet());
            }
        }
        return sets.stream().map(ArrayList::new).collect(Collectors.toList());
    }

    private static Set<Integer> generateSingleSet() {
        List<Integer> copyU = new ArrayList<>(collectionU);
        Set<Integer> set = new HashSet<>();
        int size = random.nextInt(1, SIZE_OF_U) /LEVEL_OF_COVERAGE;
        while (set.size() < size) {
            set.add(copyU.remove(random.nextInt(0, copyU.size())));
        }
        return set;
    }

}
