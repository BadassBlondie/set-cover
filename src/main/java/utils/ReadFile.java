package utils;

import java.io.*;
import java.net.URL;
import java.util.*;

public class ReadFile {

    private static final String CONFIG_FILE = "new.properties";

    public static String getProperty(String key){
        Properties properties = new Properties();
        try (InputStream input = ReadFile.class.getClassLoader().getResourceAsStream(CONFIG_FILE)) {
            properties.load(input);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties.getProperty(key);
    }

    public static List<Integer> initU(){
        String uz = ReadFile.getProperty("uZ");
        List<Integer> UZ = new ArrayList<>();
        StringBuilder builder = new StringBuilder();
        int i=0;
        while(i < uz.length()) {
            if (uz.charAt(i) != ',') {
                builder.append(uz.charAt(i));
            }else
            {
                UZ.add(Integer.parseInt(builder.toString()));
                builder.setLength(0);
                builder = new StringBuilder();
            }
            i++;
        }
        return UZ;
    }

    public static List<List<Integer>> initS(){

        List<List<Integer>> SZ = new ArrayList<>();
        StringBuilder builder = new StringBuilder();
        List<Integer> US;
        int i;

        for (int j = 1; j <= 4; j++){
            String us = ReadFile.getProperty("s"+j);
            US = new ArrayList<>();
            i=0;
            while(i < us.length()) {
                if (us.charAt(i) != ',') {
                    builder.append(us.charAt(i));
                }else
                {
                    US.add(Integer.parseInt(builder.toString()));
                    builder.setLength(0);
                    builder = new StringBuilder();
                }
                i++;
            }
            SZ.add(US);
        }

        return SZ;
    }

}
